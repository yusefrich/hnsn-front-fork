var webpack = require('webpack')
var path = require('path')
var PrerenderSpaPlugin = require('prerender-spa-plugin')
var Renderer = PrerenderSpaPlugin.PuppeteerRenderer

module.exports = {
  devServer: {
    proxy: {
      '/v1': {
        target: process.env.API_URL,
        secure: false,
        changeOrigin: true
      }
    }
  },
	css: {
		loaderOptions: {
			sass: {
				data: "@import '~@/stylesheets/core.scss';",
				includePaths: [
					require("bourbon-neat").includePaths,
				]
			}
		}
	},
	chainWebpack: config => {
		if(config.plugins.has('extract-css')) {
			const extractCSSPlugin = config.plugin('extract-css')
			extractCSSPlugin && extractCSSPlugin.tap(() => [{
				filename: 'css/[name].css',
				chunkFilename: '[name].css'
			}])
		}
	},
	configureWebpack: {
		output: {
			filename: 'js/[name].js',
			chunkFilename: '[name].js',
			publicPath: process.env.NODE_ENV == 'development' ? '/' : '/'
		},
		module: {
			rules: [
				//
			]
		},
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          'API_URL': JSON.stringify(process.env.NODE_ENV == 'development' ? 'http://localhost:8080' : process.env.API_URL),
        }
      })
    ]
	}
}
