// import Alert from './Alert'
// import Toolbar from './Toolbar'
// import Slideshow from './Slideshow'
// import Map from './Map'
// import Video from './Video'
// import Modal from './Modal'
// import Wave from './Wave'
// import Circle from './Circle'
// import Message from './Message'
// import Accordion from './Accordion'
// import { NavSocial, NavTabs, NavLang, NavList, NavCurrency } from './nav'
// import { Card, CardUrban, CardProperty, CardMedia, CardDream, CardArchitect, CardDesigner } from './card'
// import { Button, ButtonIcon, ButtonHamburger } from './button'
// import { InputText, InputSelect, InputFile, InputTextarea } from './input'
// import { GallerySlideshow, GalleryMosaic } from './gallery'
// import { ContactAction, ContactAddress, ContactNav, ContactProperty } from './contact'
// import { PropertyDatasheet, PropertyExtra, PropertyLocation, PropertyGallery, PropertyStage, PropertyProgress } from './property'

export default function install(Vue) {
	// Vue.component('ub-alert', Alert)
	// Vue.component('ub-map', Map)
	// Vue.component('ub-video', Video)
	// Vue.component('ub-modal', Modal)
	// Vue.component('ub-toolbar', Toolbar)
	// Vue.component('ub-slideshow', Slideshow)
	// Vue.component('ub-accordion', Accordion)

	// Vue.component('ub-nav-social', NavSocial)
	// Vue.component('ub-nav-tabs', NavTabs)
	// Vue.component('ub-nav-lang', NavLang)
	// Vue.component('ub-nav-list', NavList)
	// Vue.component('ub-nav-currency', NavCurrency)

	// Vue.component('ub-contact-action', ContactAction)
	// Vue.component('ub-contact-address', ContactAddress)
	// Vue.component('ub-contact-nav', ContactNav)
	// Vue.component('ub-contact-property', ContactProperty)

	// Vue.component('ub-card', Card)
	// Vue.component('ub-card-architect', CardArchitect)
	// Vue.component('ub-card-urban', CardUrban)
	// Vue.component('ub-card-property', CardProperty)
	// Vue.component('ub-card-media', CardMedia)
	// Vue.component('ub-card-dream', CardDream)
	// Vue.component('ub-card-designer', CardDesigner)

	// Vue.component('ub-button', Button)
	// Vue.component('ub-button-icon', ButtonIcon)
	// Vue.component('ub-button-hamburger', ButtonHamburger)

	// Vue.component('ub-input-text', InputText)
	// Vue.component('ub-input-select', InputSelect)
	// Vue.component('ub-input-file', InputFile)
	// Vue.component('ub-input-textarea', InputTextarea)

	// Vue.component('ub-gallery-slideshow', GallerySlideshow)
	// Vue.component('ub-gallery-mosaic', GalleryMosaic)

	// Vue.component('ub-property-datasheet', PropertyDatasheet)
	// Vue.component('ub-property-extra', PropertyExtra)
	// Vue.component('ub-property-location', PropertyLocation)
	// Vue.component('ub-property-gallery', PropertyGallery)
	// Vue.component('ub-property-stage', PropertyStage)
	// Vue.component('ub-property-progress', PropertyProgress)

	// Vue.component('ub-wave', Wave)
	// Vue.component('ub-circle', Circle)
	// Vue.component('ub-message', Message)
}
