import InputText from './InputText'
import InputSelect from './InputSelect'
import InputFile from './InputFile'
import InputTextarea from './InputTextarea'

export {
	InputText,
	InputSelect,
	InputFile,
	InputTextarea
}
