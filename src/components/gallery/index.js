import GallerySlideshow from './GallerySlideshow'
import GalleryMosaic from './GalleryMosaic'

export {
	GallerySlideshow,
	GalleryMosaic
}
