import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    videos: []
  },
  getters: {
    getVideos(state) {
      return state.videos
    }
  },
  mutations: {
  },
  actions: {
    fetchVideos({state}){
      return new Promise((resolve, reject) => {
        if (state.videos.length > 0)
          return resolve(state.videos)

        Vue.http.get(`${process.env.API_URL}/v1/videos`)
          .then(response => {
            if (response.status == 200) {
              state.videos = response.data
              return resolve(state.videos)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
  }
}
