import Vue from 'vue'


/**
*  SerializeData, pecorre todos os campos do form e gera um object FormData
*/
export function serializeData(fields) {

  let data = new FormData()

  for (let name in fields) {
    let value = fields[name]
    data.append(name, value)
  }

  return data
}

export default {
  namespaced: true,
  state: {
    retorno: ''
  },
  getters: {},
  mutations: {
  },
  actions: {
    onSave({state}, payload){
      return new Promise((resolve, reject) => {
        if (state.retorno)
          return resolve(state.retorno)
        Vue.http.post(`${process.env.API_URL}/v1/paciente-internacional`, serializeData(payload))
        .then(response => {
          if (response.status == 200) {
            state.retorno = response.data
            return resolve(state.retorno)
          }
          return resolve([])
        }, error => resolve([]))
      })
    },
  }
}
