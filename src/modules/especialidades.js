import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    especialidades: [],
		keyword: ''
  },
  getters: {
    getEspecialidades(state) {
      return state.especialidades
    },
		getKeyword(state) {
			return state.keyword
		}
  },
  mutations: {
		setKeyword(state, value) {
			return state.keyword = value
		}
  },
  actions: {
    fetchEspecialidades({state}){
      return new Promise((resolve, reject) => {
        if (state.especialidades.length > 0)
          return resolve(state.especialidades)

        Vue.http.get(`${process.env.API_URL}/v1/especialidades`)
          .then(response => {
            if (response.status == 200) {
              state.especialidades = response.data
              return resolve(state.especialidades)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
  }
}
