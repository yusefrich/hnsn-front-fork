import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    servicos: []
  },
  getters: {
    getServicos(state) {
      return state.servicos
    }
  },
  mutations: {
  },
  actions: {
    fetchServicos({ state }) {
      return new Promise((resolve, reject) => {
        if (state.servicos.length > 0)
          return resolve(state.servicos)

        Vue.http.get(`${process.env.API_URL}/v1/servicos`)
          .then(response => {
            if (response.status == 200) {
              state.servicos = response.data
              return resolve(state.servicos)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
  }
}
