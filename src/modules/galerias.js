import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    images: [
      { url: 'https://via.placeholder.com/600x400' },
            { url: 'https://via.placeholder.com/600x400' },
            { url: 'https://via.placeholder.com/600x400' },
            { url: 'https://via.placeholder.com/600x400' },
            
    ]
  },
  getters: {
    getImages(state) {
      return state.images
    },

  },
  actions: {
    fetchGalerias({state}){
      return new Promise((resolve, reject) => {
        if (state.images.length > 0)
          return resolve(state.images)

        Vue.http.get(`${process.env.API_URL}/v1/galerias`)
          .then(response => {
            if (response.status == 200) {
              state.images = response.data
              return resolve(state.images)
            }
            return resolve([])
          }, error => resolve([]))
      })
    }
  },
}
