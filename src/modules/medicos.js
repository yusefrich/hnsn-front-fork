import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    medicos: [],
    medico: '',
		keyword: ''
  },
  getters: {
    getMedicos(state) {
      return state.medicos
    },
		getKeyword(state) {
			return state.keyword
		}
  },
  mutations: {
		setKeyword(state, value) {
			state.keyword = value
		}
  },
  actions: {
    fetchMedico({state}, payload){
      return new Promise((resolve, reject) => {
        if (state.medico)
          return resolve(state.medico)

        Vue.http.get(`${process.env.API_URL}/v1/medicos/${payload.id}`)
          .then(response => {
            if (response.status == 200) {
              state.medicos = response.data
              return resolve(state.medicos)
            }
            return reject(response)
          }, error => reject(error))
      })
    },
    fetchMedicos({state}){
      return new Promise((resolve, reject) => {
        if (state.medicos.length > 0)
          return resolve(state.medicos)

        Vue.http.get(`${process.env.API_URL}/v1/medicos`)
          .then(response => {
            if (response.status == 200) {
              state.medicos = response.data
              return resolve(state.medicos)
            }
            return resolve([])
          }, error => resolve([]))
      })
    }
  },
}
