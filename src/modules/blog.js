import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    posts: [],
    featured: [],
    post: ''
  },
  getters: {
    getPosts(state) {
      return state.posts
    },
    getFeatured(state) {
      return state.featured
    },
  },
  mutations: {
  },
  actions: {
    fetchPost({state}, payload){
      return new Promise((resolve, reject) => {
        if (state.post)
          return resolve(state.post)

        Vue.http.get(`${process.env.API_URL}/v1/blog/post/${payload.slug}`)
          .then(response => {
            if (response.status == 200) {
              state.posts = response.data
              return resolve(state.posts)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchHomePosts({state}){
      return new Promise((resolve, reject) => {
        if (state.posts.length > 0)
          return resolve(state.posts)

        Vue.http.get(`${process.env.API_URL}/v1/blog/posts/6`)
          .then(response => {
            if (response.status == 200) {
              state.posts = response.data
              return resolve(state.posts)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchPosts({state}){
      return new Promise((resolve, reject) => {
        if (state.posts.length > 0)
          return resolve(state.posts)

        console.log('fetch being called');

        Vue.http.get(`${process.env.API_URL}/v1/blog/posts`)
          .then(response => {
            if (response.status == 200) {
              state.posts = response.data
              return resolve(state.posts)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchPostsByEspecialities({state}, id){
      return new Promise((resolve, reject) => {
        if (state.posts.length > 0)
          return resolve(state.posts)

        console.log('fetch being called');

        Vue.http.get(`${process.env.API_URL}/v1/blog/posts?especiality=${id}`)
          .then(response => {
            if (response.status == 200) {
              state.posts = response.data
              return resolve(state.posts)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchFeatured({ state }) {
      return new Promise((resolve, reject) => {
        if (state.featured.length > 0)
          return resolve(state.featured)

        Vue.http.get(`${process.env.API_URL}/v1/blog/posts/featured`)
          .then(response => {
            if (response.status == 200) {
              state.featured = response.data
              return resolve(state.featured)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
  }
}
