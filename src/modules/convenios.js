import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    convenios: [],
    states:[],
    plans:[],
    list:null,
  },
  getters: {
    getConvenios(state) {
      var data = {
        'maternidade': state.convenios.filter(convenio => {
          return convenio.tipo == 'Maternidade'
        }),
        'adultos': state.convenios.filter(convenio => {
          return convenio.tipo == 'Adultos'
        }),
        'pediatria': state.convenios.filter(convenio => {
          return convenio.tipo == 'Pediatria'
        }),
      }

      return data
    }
  },
  mutations: {
  },
  actions: {
    fetchConvenios({ state }) {
      return new Promise((resolve, reject) => {
        if (state.convenios.length > 0)
          return resolve(state.convenios)

        Vue.http.get(`${process.env.API_URL}/v1/convenios`)
          .then(response => {
            if (response.status == 200) {
              state.convenios = response.data
              console.log(response.data)
              return resolve(state.convenios)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchStates({state}){
      return new Promise((resolve, reject) => {
        if (state.states.length > 0)
          return resolve(state.states)

        Vue.http.get(`${process.env.API_URL}/v1/unidades/states`)
          .then(response => {
            if (response.status == 200) {
              state.states = response.data
              return resolve(state.states)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchPlans({state}){
      console.log("fetchPlans being called")
      return new Promise((resolve, reject) => {
        if (state.plans.length > 0)
          return resolve(state.list)

        Vue.http.get(`${process.env.API_URL}/v1/convenios/list`)
          .then(response => {
            if (response.status == 200) {
              state.plans = response.data
              return resolve(state.plans)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
    fetchList({state}, payload){
      console.log("fetchList being called")

      return new Promise((resolve, reject) => {
        Vue.http.get(`${process.env.API_URL}/v1/unidades/list?state=${payload.state}&plan=${payload.plan}`)
          .then(response => {
            if (response.status == 200) {
              state.list = response.data
              return resolve(state.list)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },

  }
}
