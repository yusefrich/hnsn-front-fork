import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    parceiros: []
  },
  getters: {
    getParceiros(state) {
      return state.parceiros
    }
  },
  mutations: {
  },
  actions: {
    fetchParceiros({ state }) {
      return new Promise((resolve, reject) => {
        if (state.parceiros.length > 0)
          return resolve(state.parceiros)

        Vue.http.get(`${process.env.API_URL}/v1/parceiros`)
          .then(response => {
            if (response.status == 200) {
              state.parceiros = response.data
              return resolve(state.parceiros)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
  }
}
