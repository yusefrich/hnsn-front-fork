import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    retorno: ''
  },
  getters: {
    getVideos(state) {
      return state.videos
    }
  },
  mutations: {
  },
  actions: {
    sendNewsletter({state}, payload){
      return new Promise((resolve, reject) => {
        if (state.retorno)
          return resolve(state.retorno)

        Vue.http.post(`${process.env.API_URL}/v1/newsletter/create`, {name: payload.name, email: payload.email})
          .then(response => {
            if (response.status == 200) {
              state.retorno = response.data
              return resolve(state.retorno)
            }
            return resolve([])
          }, error => resolve([]))
      })
    },
  }
}
