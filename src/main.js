import Vue from 'vue'
import VueResource from 'vue-resource'
import VueMeta from 'vue-meta'
import VeeValidate from 'vee-validate'
import NProgress from 'nprogress'
import router from './router'
import store from './store'
import loadGoogleMapsApi from 'load-google-maps-api'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueFullPage from 'vue-fullpage.js'
import VueTheMask from 'vue-the-mask'

import Lang, { i18n } from './lang'
import Components from './components'
import Directives from './directives'
import './stylesheets/main.scss'
import 'nprogress/nprogress.css'

import VueAnalytics from 'vue-analytics'
import Meta from 'vue-meta'

import moment from 'moment'
import 'moment/locale/pt-br'

import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'

Vue.use(VueAnalytics, {
  id: 'UA-141653435-1',
  router,
  checkDuplicatedScript: true
})

var VueScrollTo = require('vue-scrollto')

Vue.use(VueScrollTo)
Vue.use(Meta)
Vue.use(VueResource)
Vue.use(VueMeta)
Vue.use(VeeValidate)
Vue.use(Lang)
Vue.use(Directives)
Vue.use(Components)
Vue.use(VueAwesomeSwiper)
Vue.use(VueFullPage)
Vue.use(PerfectScrollbar)
Vue.use(VueTheMask)

Vue.config.productionTip = false

Vue.http.options.emulateJSON = true

Vue.http.interceptors.push((request, next) => {
	NProgress.start()
	request.headers.set('Content-Language', i18n.locale)

	next(response => {
		NProgress.done()
		return response
	})
})

new Vue({
	router,
	store,
	i18n,
	render: h => h('router-view'),
	mounted() {
		// Initialize Google Maps
		loadGoogleMapsApi({
			key: process.env.MAPS_KEY
		})
			.then(maps => {
				this.$root.$emit('maps-init', maps)
			})
	}
})
	.$mount('#app')
