export default {
	home: 'Home',
	about: 'About Urban',
	properties: 'Want to Live',
	dreams: 'My Urban',
	blog: 'Urban Space',
	jp: 'João Pessoa',
	sales: 'Your place on Brazil',
	contact: 'Contact',
	architects: 'Architects'
}
