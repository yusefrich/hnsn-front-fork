export default {
	title: 'Your place on Brazil',
	text: 'Urban is focused on purposing the better experience of live to every moment in life. It`s here that your future happens.',
	documentation: {
		title: 'Documentation',
		text: 'Saiba de quais documentos você precisará para comprar um de nossos apartamentos.',
		list: {
			'Compra de apartamentos para estrangeiros': [
				'Selecione uma moeda para exibir a quantia',
				'Documentação do comprador',
				'Documentação do procurador'
			],
			'Compra de apartamentos para brasileiros morando fora do Brasil': [
				'Procuração detalhada a um brasileiro, com poderes para o representar legalmente',
				'Documentação do comprador',
				'Documentação do procurador'
			]
		}
	},
	contact: {
		title: 'Fale com a gente de qualquer lugar do mundo e a qualquer momento, por uma dessas maneiras.',
	},
	blog: {
		title: 'João Pessoa, experiência em qualidade de vida.'
	}
}
