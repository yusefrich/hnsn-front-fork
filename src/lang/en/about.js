export default {
	tabs: {
		belief: 'Belief',
		values: 'Values',
		purpose: 'Purpose'
	},

	belief: {
		title: 'You and the world around you in a perfect harmony',
		body: 'Urban understands that every person has its own singularity while the same time live collectively. We believe that we can provide our best environments so you can live a complete life experience, integrating your singularity with the community you are in.',
		list: {
			cooperation: 'Cooperation',
			sharing: 'Sharing',
			connectivity: 'Connectivity',
			integration: 'Integration'
		},
		goal: 'Our goal is to create experiences for the individuals which are part of our comunities. We look to provide the best for each person and, at the same time, motivate our comunity based on collective concepts, attending our individuals needs and posses the capacity to strenghthen the human relations, transforming the environment into a place more sustainable and integrated.',
		slideshow: [
			{
				title: 'We love the new',
				body: 'We are always looking for housing solutions that will propose the possibility to live the best proper experience to your life moment.',
				background: require('@/assets/images/delete/property-02.jpg')
			},
			{
				title: 'Amamos o novo',
				body: 'Estamos sempre buscando soluções de moradia que proporcionarão a possibilidade de viver a melhor experiência adequada ao seu momento de vida.',
				background: require('@/assets/images/delete/property-03.jpg')
			}
		],
		care: 'We love to discover what could be better for your.',
		services: {
			title: 'Our services',
			body: 'We think on every single detail to facilitate your life and transmit that through our services. It`s them which helps to create unforgetable experiences.',
			text: 'At Urban you dont find just a property to live. You find a mix full of possibilities and experiences.',
			featured: 'Place and life!',
			small: 'Services liable to change, dynamic, as the interests of the group/property.'
		}
	},

	values: {
		title: 'Soluções em experiências de moradia',
		body: 'Viver em um Urban vai além de morar em um empreendimento. São as experiências que fazem a diferença. É por isso que o nosso maior propósito é oferecer a melhor experiência de viver para os diversos momentos da vida, buscando atender cada necessidade e gerar comunidades conectadas e sustentáveis.',
		slideshow: [
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
		]
	},

	purpose: {
		slideshow: [
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			},
			{
				title: 'Simplicidade',
				text: 'É nos detalhes que buscamos fazer a diferença, mostrando que podemos trazer profundo significado para as coisas mais simples.',
				thumbnail: 'http://picsum.photos/600/400'
			}
		]
	},
}
