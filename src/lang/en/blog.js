export default {
	tabs: {
		urban: 'Contents',
		events: 'Events'
	},
	more: 'Load more',
	related: 'Related'
}
