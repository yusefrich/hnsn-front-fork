export default {
	card: {
		urban: {
			title: '<span>We exist<br /> to offer</span> <span>better life experiences to people</span>',
			body: 'Urban understands that every person has its own singularity while the same time live collectively.',
			more: 'Meet Urban'
		},
		property: {
			more: 'More about'
		},
		dream: {
			text: 'Percentage of how much the apartment has with your choices',
			decoration: 'Decoration',
			shade: 'Shade',
			furniture: 'Furniture',
			architect: 'Architect',
			rooms: 'rooms',
			suites: 'suites',
			more: 'Make my dreams come true',
		}
	},

	video: {
		watch: 'Watch video'
	},

	property: {
		location: {
			title: 'Location',
			text: 'Reach us and we`ll be glad to help you.'
		},
		extra: {
			title: 'Differential'
		},
		datasheet: {
			title: 'Datasheet'
		}
	},

	contact: {
		nav: {
			sale: {
				title: 'Buy a Urban',
				subtitle: 'Sales'
			},
			work: {
				title: 'Become Urban',
				subtitle: 'Work with us'
			},
			talk: {
				title: 'Take a doubt',
				subtitle: 'Talk to us'
			}
		},
		address: {
			title: 'Where are we?'
		}
	},
	
	nav: {
		currency: {
			title: 'Select a currency to show its value'
		}
	}
}
