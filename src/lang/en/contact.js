export default {
	sales: {
		title: 'I want to buy<br /> a Urban',
		subtitle: 'Talk to us'
	},
	talk: {
		title: 'I want to take<br /> a doubt',
		subtitle: 'Talk to us'
	},
	work: {
		title: 'I want to be Urban',
		subtitle: 'Talk to us'
	},
	send: 'Send',
	success: {
		title: 'Message sent successfully!',
		subtitle: 'Message sent'
	}
}
