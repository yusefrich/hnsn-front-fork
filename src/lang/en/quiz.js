export default {
	questions: [
		'Which decoration combines<br /> the most with you?',
		'Which shading you<br /> most identify?',
		'Which furniture cannot<br /> miss in you apartment?',
		'Which furniture cannot<br /> miss in you apartment?',
	],
	topics: {
		size: 'Size',
		location: 'Location',
		attribute: 'Attribute',
		decoration: 'Decoration'
	},
	next: 'Next'
}
