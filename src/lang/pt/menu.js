export default {
	home: 'Home',
	about: 'A Urban',
	properties: 'Quero Morar',
	dreams: 'My Urban',
	blog: 'Conteúdos',
	jp: 'João Pessoa',
	sales: 'Seu lugar no Brasil',
	contact: 'Contato',
	architects: 'Arquitetos'
}
