export default {
	tabs: {
		urban: 'Conteúdos',
		events: 'Eventos'
	},
	more: 'Ver mais',
	related: 'Relacionados'
}
