export default {
	sales: {
		title: 'Quero comprar<br /> um Urban',
		subtitle: 'Fale Conosco'
	},
	talk: {
		title: 'Quero tirar<br /> uma dúvida',
		subtitle: 'Fale conosco'
	},
	work: {
		title: 'Quero ser Urban',
		subtitle: 'Fale Conosco'
	},
	send: 'Enviar',
	success: {
		title: 'Mensagem enviada com sucesso!',
		subtitle: 'Mensagem enviada'
	}
}
