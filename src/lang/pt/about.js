export default {
	tabs: {
		belief: 'Crença',
		values: 'Valores',
		purpose: 'Propósito'
	},

	belief: {
		title: 'Você e o mundo ao seu redor em perfeita sintonia',
		body: 'A Urban entende que cada pessoa possui sua individualidade e ao mesmo tempo vive coletivamente. Nós acreditamos que podemos proporcionar os melhores ambientes para que você viva experiências de vida completas, integrando a sua individualidade com a comunidade em que você está.',
		list: {
			cooperation: 'Cooperação',
			sharing: 'Compartilhamento',
			connectivity: 'Conectividade',
			integration: 'Integração'
		},
		goal: 'Nosso objetivo é gerar experiências junto aos indivíduos que fazem parte das nossas comunidades. Nós buscamos proporcionar o melhor para cada pessoa e, ao mesmo tempo, motivar uma comunidade baseada em conceitos coletivos, que têm suas necessidades individuais atendidas e possuem a capacidade de fortalecer as relações humanas, transformando o ambiente em um espaço mais sustentável e integrado.',
		slideshow: [
			{
				title: 'Amamos o novo',
				body: 'Estamos sempre buscando soluções de moradia que proporcionarão a possibilidade de viver a melhor experiência adequada ao seu momento de vida.',
				background: require('@/assets/images/img_amamos-o-novo.jpg')
			},
			{
				title: 'O design',
				body: 'Nós acreditamos em um mundo com mais conexões, onde os espaços sejam capazes de proporcionar experiências de vida para as pessoas, através da arquitetura e de um design funcional.',
				background: require('@/assets/images/img_Design.jpg')
			},
			{
				title: 'Tecnologia',
				body: 'Nós buscamos formas diferentes de usar a tecnologia a nosso favor, encontrando formas de aplicar praticidade aos detalhes, trazendo um aumento na qualidade de vida e elevando a sua moradia a um espaço com experiências de vida muito mais agradáveis.',
				background: require('@/assets/images/img_tecnologia.jpg')
			}
		],
		care: 'Amamos descobrir o que pode ser melhor para você.',
		services: {
			title: 'Nossos serviços',
			body: 'Nós pensamos em cada detalhe para facilitar a sua vida e transmitimos isso através dos nossos serviços. São eles que ajudam a gerar experiências inesquecíveis.',
			text: 'Na Urban você não encontra apenas um empreendimento para morar. Você encontra um mix cheio de possibilidades e experiências.',
			featured: 'Lugar e vida!',
			small: 'Serviços passiveis de mudança, dinâmicos, conforme os interesses do grupo/empreendimento.'
		},
	},

	values: {
		title: 'Soluções em experiências de moradia',
		body: 'Viver em um Urban vai além de morar em um empreendimento. São as experiências que fazem a diferença. É por isso que o nosso maior propósito é oferecer a melhor experiência de viver para os diversos momentos da vida, buscando atender cada necessidade e gerar comunidades conectadas e sustentáveis.',
		slideshow: [
			{
				title: 'Humildade',
				text: 'Temos a consciência e a liberdade de refletir sobre nossas ações, reconhecendo nossos pontos fortes e fracos, sempre nos colocando no lugar do outro.',
				thumbnail: require('@/assets/images/Humildade.jpg')
			},
			{
				title: 'Sustentabilidade',
				text: 'Acreditamos que o sucesso de nossas ações do hoje só terão efeito duradouro se planejarmos o presente com foco no futuro e em suas implicações.',
				thumbnail: require('@/assets/images/Sustentabilidade.jpg')
			},
			{
				title: 'Melhoria Contínua',
				text: 'Os nossos esforços estão sempre voltados para oferecer o melhor para nossos clientes e encontrar formas de superar o que já foi realizado.',
				thumbnail: require('@/assets/images/Melhoria_Continua.jpg')
			},
			{
				title: 'Gentileza',
				text: 'É através da gentileza que encontramos formas de integrar pessoas e espaços, planejando ações transformadoras e capazes de trazer novas possibilidades de vivências.',
				thumbnail: require('@/assets/images/Gentileza.jpg')
			},
			{
				title: 'Inovação Constante',
				text: 'O futuro é a nossa maior inspiração e estamos sempre buscando seguir e criar novas tendências que tragam inovação.',
				thumbnail: require('@/assets/images/Inovacao.jpg')
			},
		]
	},

	purpose: {
		
	},
}
