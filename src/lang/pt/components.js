export default {
	card: {
		urban: {
			title: '<span>Existimos<br /> para oferecer</span> <span>experiências melhores de vida para as pessoas</span>',
			body: 'A Urban entende que cada pessoa possui sua individualidade e ao mesmo tempo vive coletivamente.',
			more: 'Conheça o Urban'
		},
		property: {
			more: 'Saiba mais'
		},
		dream: {
			text: 'Porcetagem do quanto o apartamento tem a ver com as suas escolhas',
			decoration: 'Decoração',
			shade: 'Tonalidade',
			furniture: 'Móveis',
			architect: 'Arquiteto',
			rooms: 'quartos',
			suites: 'suítes',
			more: 'Realizar meu sonho'
		}
	},

	video: {
		watch: 'Assistir vídeo'
	},

	property: {
		location: {
			title: 'Localização',
			text: 'Entre em contato conosco e teremos todo prazer em ajudar.'
		},
		extra: {
			title: 'Diferenciais'
		},
		datasheet: {
			title: 'Ficha Técnica'
		}
	},

	contact: {
		nav: {
			sale: {
				title: 'Quero comprar um Urban',
				subtitle: 'Vendas'
			},
			work: {
				title: 'Quero ser Urban',
				subtitle: 'Trabalhe conosco'
			},
			talk: {
				title: 'Quero tirar uma dúvida',
				subtitle: 'Fale conosco'
			}
		},
		address: {
			title: 'Onde Estamos'
		}
	},

	nav: {
		currency: {
			title: 'Selecione uma moeda para exibir a quantia'
		}
	}
}
