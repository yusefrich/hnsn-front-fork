export default {
	questions: [
		'Para você, qual é o tamanho<br /> ideal de imóvel?',
		'Praia ou cidade?',
		'Do que você não abre mão?',
		'Qual a estética arquitetônica<br /> combina mais com você?',
	],
	topics: {
		size: 'Tamanho',
		location: 'Localização',
		attribute: 'Atributos',
		decoration: 'Decoração'
	},
	next: 'Próximo'
}
