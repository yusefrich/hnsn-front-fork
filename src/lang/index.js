import VueI18n from 'vue-i18n'

let i18n = null

export default function install(Vue) {
	Vue.use(VueI18n)

	i18n = new VueI18n({
		locale: 'pt',
		messages: {
			en: require('./en').default,
			pt: require('./pt').default
		}
	})
}

export { i18n }
