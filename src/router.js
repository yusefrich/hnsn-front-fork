import Vue from 'vue'
import Router from 'vue-router'
import App from './App'
import Home from './views/Home'
import QuemSomos from './views/QuemSomos'
import Especialidades from './views/Especialidades'
import EspecialidadeDetalhe from './views/EspecialidadeDetalhe'
import Medicos from './views/Medicos'
import Medico from './views/Medico'
import Servicos from './views/Servicos'
import Galerias from './views/Galerias'
import Noticias from './views/Noticias'
import Convenios from './views/Convenios'
import Noticia from './views/Noticia'
import Acesso from './views/Acesso'
import FormularioMedico from './views/FormularioMedico'
import Ouvidoria from './views/Ouvidoria'
import Internacional from './views/PacienteInternacional'
import Orcamento from './views/Orcamento'

import { i18n } from '@/lang'
import store from '@/store'

Vue.use(Router)

const router = new Router({
	mode: 'history',
  routes: [
		{
			path: '/',
			component: App,
			children: [
				{
					path: '',
					name: 'home',
					component: Home
				},
				{
					path: 'quem-somos',
					name: 'quem-somos',
					component: QuemSomos
				},
				{
					path: 'especialidades-e-medicos',
					name: 'especialidades-e-medicos',
					component: Especialidades,
					children: [
						{
							path: 'medicos',
							redirect: 'medicos/anestesia'
						},
						{
							path: 'medicos/:slug?',
							name: 'medicos',
							component: Medicos
						},
						{
							path: 'detalhe/:slug?/:id?',
							name: 'detalhes',
							component: EspecialidadeDetalhe
						},
						{
							path: 'detalhe/anestesia/12',
							name: 'especialidadesDetalhes',
							component: EspecialidadeDetalhe
						},
						{
							path: 'medico/:id',
							name: 'medico',
							component: Medico
						}
					]
				},
				{
					path: 'servicos',
					name: 'servicos',
					component: Servicos
				},
				{
					path: 'convenios',
					name: 'convenios',
					component: Convenios
				},
				{
					path: 'noticias',
					name: 'noticias',
					component: Noticias
				},
				{
					path: 'noticia/:slug',
					name: 'noticia',
					component: Noticia
				},
				{
					path: 'acesso',
					name: 'acesso',
					component: Acesso
				},
				{
					path: 'acesso/form',
					name: 'acesso-form',
					component: FormularioMedico
				},
				{
					path: 'galerias',
					name: 'galerias',
					component: Galerias
				},
				{
					path: 'ouvidoria',
					name: 'ouvidoria',
					component: Ouvidoria,
				},
				{
					path: 'internacional',
					name: 'internacional',
					component: Internacional,
				},
				{
					path: 'orcamento',
					name: 'orcamento',
					component: Orcamento,
				}
			]
		}
  ]
})

router.afterEach((to, from) => {
	document.dispatchEvent(new Event('app.rendered'))
})

export default router
