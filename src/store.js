import Vue from 'vue'
import Vuex from 'vuex'
import convenios from './modules/convenios'
import blog from './modules/blog'
import videos from './modules/videos'
import especialidades from './modules/especialidades'
import medicos from './modules/medicos'
import galerias from './modules/galerias'
import servicos from './modules/servicos'
import parceiros from './modules/parceiros'
import newsletter from './modules/newsletter'
import ouvidoria from './modules/ouvidoria'
import internacional from './modules/internacional'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		convenios,
		blog,
		videos,
    especialidades,
    medicos,
    galerias,
    servicos,
    parceiros,
    newsletter,
	ouvidoria,
	internacional
	},
	state: {
		viewport: 'desktop',
		width: 0,
		height: 0,
	},
	getters: {
		getWidth(state) {
			return state.width
		},
		getHeight(state) {
			return state.height
		},
		getViewport(state) {
			return state.viewport
		}
	},
	mutations: {
		updateViewport(state, payload) {
			state.width = payload.width
			state.height = payload.height

			if (state.width <= 768)
				state.viewport = 'smartphone'
			else
				state.viewport = 'desktop'
		},

		clearStates(state) {
			state.property.properties = []
			state.blog['all-urban'].featured = []
			state.blog['all-urban'].posts = []
			state.blog['events'].featured = []
			state.blog['events'].posts = []
			state.blog['joao-pessoa'].featured = []
			state.blog['joao-pessoa'].posts = []
		}
	},

	actions: {

	},
})
